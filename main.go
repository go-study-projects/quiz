package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"time"
)

func main() {
	timeLimit := flag.Int("limit", 30, "time limit in seconds (default 30)")
	csvFile := flag.String("csv", "problems.csv", "csv file with problems")
	flag.Parse()

	file, err := os.Open(*csvFile)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()

	reader := csv.NewReader(file)
	lines, err := reader.ReadAll()
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}

	answeredCorrect := 0
	totalProblems := len(lines)

	go watchTimeLimit(*timeLimit, &answeredCorrect, &totalProblems)

	for i, record := range lines {
		question := record[0]
		answer := record[1]
		var response string

		fmt.Printf("Problem #%d: %s = ", i, question)
		fmt.Scan(&response)
		if response == answer {
			answeredCorrect++
		}
	}

	fmt.Printf("You scored %d out of %d.\n", answeredCorrect, totalProblems)
}

func watchTimeLimit(limit int, answeredCorrect *int, totalProblems *int) {
	start := time.Now()
	maxTime := start.Add(time.Second * time.Duration(limit))
	for {
		if time.Now().After(maxTime) {
			fmt.Printf(
				"\nYou scored %d out of %d.\n",
				*answeredCorrect, *totalProblems)
			os.Exit(0)
		}
	}
}
